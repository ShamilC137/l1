package tasks

import (
	"fmt"
	"os"
	"syscall"
	"time"
)

// Проблема первой функции: активное ожидание (горутина борется за ресурсы, хотя ничего хорошего она не делает)

// Проблема второй функции: ломается планировщик go и ожидание идет на уровне процесса. Эту проблему можно обойти только
// в том случае, если явно создать поток при помощи вызова к ОС и, зная id нового потока, его можно будет "правильно"
// приостановить

// На линуксе можно написать аналогичную второй функции функцию, использую futex

// Active waiting
func ActiveSleep(duration time.Duration) {
	end := time.Now().Add(duration)

	for time.Now().Before(end) {

	}
}

// on linux we can use futex syscall
func PassiveSleep(duration time.Duration) {
	processHandler, err := syscall.GetCurrentProcess()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	state, err := syscall.WaitForSingleObject(processHandler, uint32(duration/time.Millisecond))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if state != syscall.WAIT_TIMEOUT {
		fmt.Println("Unexpected error")
		os.Exit(1)
	}
}

func wait(function func(time.Duration), duration time.Duration) {
	t1 := time.Now()
	function(duration)
	t2 := time.Now()
	fmt.Println(t2.Sub(t1))
}

func Task25() {
	wait(ActiveSleep, 1*time.Second)

	wait(PassiveSleep, 1*time.Second)
}
