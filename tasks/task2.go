package tasks

import "fmt"

func SquaresCalculateAwaiting(slice []int) {
	length := len(slice)

	c := make(chan int, length)
	go SquaresCalculate(slice, c)

	for index := 0; index < length; index++ {
		fmt.Println(<-c)
	}
}

func SquaresCalculate(values []int, c chan int) {
	for _, value := range values {
		c <- value * value
	}
}

func Task2() {
	slice := []int{2, 4, 6, 8, 10}
	SquaresCalculateAwaiting(slice)
}
