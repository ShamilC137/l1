package tasks

import "fmt"

func nearestLeftBound(value float32) int {
	trunc := int(value)

	return trunc - (trunc % 10)
}

func GroupTemp(sequence []float32) map[int][]float32 {
	groupedData := make(map[int][]float32)

	for _, value := range sequence {
		key := nearestLeftBound(value)
		groupedData[key] = append(groupedData[key], value)
	}

	return groupedData
}

func Task10() {
	fmt.Println(GroupTemp([]float32{25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5, -25}))
}
