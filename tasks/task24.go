package tasks

import (
	"fmt"
	"math"
)

type Coordinate float32

type Point struct {
	x Coordinate
	y Coordinate
}

func NewPoint(x, y Coordinate) *Point {
	return &Point{x, y}
}

func (point *Point) DistanceTo(dest *Point) float64 {
	xdiff := float64(dest.x - point.x)
	ydiff := float64(dest.y - point.y)
	return math.Sqrt(math.Pow(xdiff, 2.) + math.Pow(ydiff, 2.))
}

func Task24() {
	first := NewPoint(1., 2.)
	second := NewPoint(1., 5.)

	fmt.Println("Distance:", first.DistanceTo(second))
}
