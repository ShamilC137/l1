package tasks

import (
	"context"
	"fmt"
	"time"
)

func DurationalWork(timeout time.Duration) {
	// подразумевается, что cancel не потребуется вызывать явно, так как нас интересует только завершение через некоторое время.
	// Можно было использовать таймер и закрыть контекст явно по его истечению, но go convention рекомендует использовать
	// для этих целей именно контекст (таймер можно использовать только в данной функции, поэтому контекст создать в
	// потребуется в любом случае)
	ctx, _ := context.WithTimeout(context.Background(), timeout)
	c := make(chan int, 16)

	go Worker(ctx, c)

	for counter := 0; ; counter++ {
		select {
		case <-ctx.Done():
			return
		case c <- counter:

		}
	}
}

// Если горутина не успеет завершить работу до окончания работы программа (т.е. выхода из main), то это проблема горутины
func Worker(ctx context.Context, c chan int) {
	for {
		select {
		case <-ctx.Done():
			return

		case val := <-c:
			fmt.Println(val)
		}
	}
}

func Task5() {
	t1 := time.Now()
	DurationalWork(2 * time.Second)
	t2 := time.Now()
	fmt.Println("Actual duration: ", t2.Sub(t1))
}
