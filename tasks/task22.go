package tasks

import (
	"fmt"
	"math/big"
)

func Task22() {
	first := big.NewInt(2 << 21)
	second := big.NewInt(2 << 22)
	result := &big.Int{}

	fmt.Println("first", first)
	fmt.Println("second", second)

	result.Mul(first, second)
	fmt.Println("Multiplication:", result)

	result.Add(result, second)
	fmt.Println("Sum:", result)

	result.Sub(result, second)
	fmt.Println("Subtract:", result)

	result.Div(result, second)
	fmt.Println("Division:", result)
}
