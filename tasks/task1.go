package tasks

import (
	"errors"
	"fmt"
)

type Metre float32
type Kilogram float32

type Human struct {
	name   string
	weight Kilogram
	height Metre
}

var WrongHumanData = errors.New("wrong data")

// NewHuman валидирует переданные аргументы
func NewHuman(name string, weight Kilogram, height Metre) (*Human, error) {
	if len(name) == 0 || weight <= 0. || height <= 0. {
		return nil, WrongHumanData
	}

	return &Human{name: name, weight: weight, height: height}, nil
}

// Так как поля класса не являются экспортируемыми (для валидации через сеттеры), для получения значения полей нужно
// использовать геттеры

func (human *Human) GetName() string {
	return human.name
}

func (human *Human) GetWeight() Kilogram {
	return human.weight
}

func (human *Human) GetHeight() Metre {
	return human.height
}

func (human *Human) SetName(name string) *Human {
	if len(name) != 0 {
		human.name = name
	}
	return human
}

func (human *Human) SetWeight(weight Kilogram) *Human {
	if weight > 0. {
		human.weight = weight
	}
	return human
}

func (human *Human) SetHeight(height Metre) *Human {
	if height > 0. {
		human.height = height
	}
	return human
}

type Action struct {
	*Human
}

// BMI рассчитывает индекс массы тела (чтобы показать, что теперь можно использовать поиск функций встроенных объектов)
func (action *Action) BMI() float32 {
	return float32(action.GetWeight()) / float32(action.GetHeight())
}

func NewAction(human *Human) *Action {
	return &Action{human}
}

func Task1() {
	human, _ := NewHuman("name", 63.9, 1.67)
	action := NewAction(human)
	action.SetName("New Name Using Action")
	fmt.Println(fmt.Sprintf("Name: %s, weight: %f, height: %f", action.GetName(), action.GetWeight(), action.GetHeight()))
	fmt.Println("Индекс массы тела: ", action.BMI())
}
