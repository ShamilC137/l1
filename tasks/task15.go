package tasks

import (
	"fmt"
	"strings"
)

func createHugeString(size int, val byte) string {
	// inefficient way
	var b = strings.Builder{}
	b.Grow(size) // recreating after each call
	for index := 0; index < size; index++ {
		b.WriteByte(val)
	}
	return b.String()
}

var justString string

func someFunc(val byte) {
	builder := strings.Builder{}
	builder.Grow(100) // если явно не указывать размер буфера, то он будет "как минимум" 100 (на самом деле - 112)

	builder.WriteString(createHugeString(1<<10, val)[:100]) // string опирается на исходный массив байт, из-за чего
	// прошлая реализация хранила не только 100 символов, но и все остальное
	justString = builder.String() // no copy
}

func Task15() {
	someFunc(48)
	fmt.Println(justString)
}
