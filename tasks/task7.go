package tasks

import "sync"

// RWMap allows multiple readers/ single writer
// Simple map (without using shards, etc.)
type RWMap struct {
	guard *sync.RWMutex
	data  map[any]any
}

func NewRWMap(guard *sync.RWMutex, data map[any]any) *RWMap {
	return &RWMap{guard: guard, data: data}
}

func (object *RWMap) Store(key, value any) {
	object.guard.Lock()
	defer object.guard.Unlock()

	object.data[key] = value
}

func (object *RWMap) Load(key any) any {
	object.guard.RLock()
	defer object.guard.RUnlock()

	return object.data[key]
}
