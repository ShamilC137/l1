package tasks

import (
	"context"
	"time"
)

// I dont care about graceful goroutine completion (i.e. they can outlive main goroutine and then be killed)

// 1 option

func ShutdownChanWorker(shutdown chan ShutdownEntity) {
	select {
	// other options

	case <-shutdown:
		return
	}
}

type ShutdownEntity struct{}

func ShutdownChanStarter() {
	shutdown := make(chan ShutdownEntity)
	ShutdownChanWorker(shutdown)

	// ...

	close(shutdown) // shutdown <- ShutdownEntity{}
}

// 2 option (questionable)

func TimeoutWorker(timeout time.Duration) {
	select {
	// other options

	case <-time.After(timeout):
		return
	}
}

func TimeoutStarter() {
	go TimeoutWorker(1 * time.Second)

	// some actions
}

// 3 option

func ContextWorker(ctx context.Context) {
	// we may check deadline here

	select {
	// other options

	case <-ctx.Done():
		return
	}
}

func ContextStarter() {
	ctx, cancel := context.WithCancel(context.Background()) // or other
	defer cancel()                                          // if this operation performed faster or WithCancel is used to create context

	go ContextWorker(ctx)

	// some actions
}
