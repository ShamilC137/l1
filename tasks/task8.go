package tasks

import "fmt"

func SetBit(source int64, pos uint8, val bool) int64 {
	// no values validate

	realPos := pos - 1
	if val {
		return source | (1 << realPos)
	} else {
		return source & ^(1 << realPos)
	}
}

func Task8() {
	source := int64(0b11101) // 29

	source = SetBit(source, 1, false) // must be 0b11100 (28)
	fmt.Println(fmt.Sprintf("After set last bit to zero: %b", source))

	source = SetBit(source, 6, true) // must be 0b111100 (60)
	fmt.Println(fmt.Sprintf("After set 6th bit to one: %b", source))
}
