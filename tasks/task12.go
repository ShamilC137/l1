package tasks

import "fmt"

func CreateSet(sequence []string) []string {
	set := make(map[string]Dummy)
	result := make([]string, 0)

	dummy := Dummy{}
	for _, value := range sequence {
		if _, found := set[value]; !found {
			set[value] = dummy
			result = append(result, value)
		}
	}

	return result
}

func Task12() {
	fmt.Println(CreateSet([]string{"cat", "cat", "dog", "cat", "tree"}))
}
