package tasks

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func DoWork(workerCount uint8) {
	// Данный контекст используется для синхронизации работы
	ctx, cancel := context.WithCancel(context.Background())

	// Используется для ожидания завершения работы воркеров
	wg := sync.WaitGroup{}
	wg.Add(int(workerCount))

	defer func() {
		cancel()
		wg.Wait() // waiting for all goroutines
	}()

	shutdown := make(chan os.Signal, 1)     // сюда будут переданы сигналы от операционной системы
	signal.Notify(shutdown, syscall.SIGINT) // обрабатываем только interrupt (ctrl+c)

	workerChan := make(chan int, workerCount) // канал для передачи данных воркерам
	for index := uint8(0); index < workerCount; index++ {
		// проверяем, что во время создания воркеров не потребовалось завершить работу
		select {
		case <-shutdown:
			return
		default:
			go WorkerRoutine(ctx, &wg, workerChan)
		}
	}

	for counter := 0; true; counter++ {
		// если сигнал о завершении работы не был послан, посылаем данные в канал воркеров
		select {
		case <-shutdown:
			return
		case workerChan <- counter: // если для отправления данных использовать default блок, то может возникнуть проблема:
			// если воркеры "слишком заняты" чтобы прочитать значения, а буфер уже заполнен, можно упустить момент с обработкой сигнала о завершении
		}
	}

}

func WorkerRoutine(ctx context.Context, wg *sync.WaitGroup, c chan int) {
	// Программа будет завершена только после того, как все воркеры закончат работу (будем считать, что в противном случае
	// мы что нибудь сломаем, хотя планировщик go может закрыть все висячие горутины сам)
	defer func() {
		wg.Done() // notify about completion
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case val := <-c:
			fmt.Println(val)
		}
	}
}

func Task4() {
	DoWork(8)
}
