package tasks

import "fmt"

func Swap[T any](lhs *T, rhs *T) {
	*lhs, *rhs = *rhs, *lhs
}

func Task13() {
	a, b := 1, 2
	fmt.Println("origin a:", a)
	fmt.Println("origin b:", b)
	Swap(&a, &b)
	fmt.Println("swapped a:", a)
	fmt.Println("swapped b:", b)
}
