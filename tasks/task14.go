package tasks

import (
	"fmt"
	"reflect"
)

// Если не использовать рефлексию, то go предоставляет  оператор switch value.(type), в котором можно сделать
// подобные действия. Проблема в том, что требуются конкретные типы, а какой именно будет иметь тип chan - неизвестно
func PrintUnderlyingType(value any) {
	switch reflect.TypeOf(value).Kind() {
	case reflect.String:
		fmt.Println("string")
	case reflect.Int:
		fmt.Println("int")
	case reflect.Bool:
		fmt.Println("bool")
	case reflect.Chan:
		fmt.Println("channel")

	default:
		fmt.Println("unknown")
	}
}

func Task14() {
	fmt.Println("Source type is 'string'")
	PrintUnderlyingType("")
	fmt.Println()

	fmt.Println("Source type is 'bool'")
	PrintUnderlyingType(true)
	fmt.Println()

	fmt.Println("Source type is 'int'")
	PrintUnderlyingType(1)
	fmt.Println()

	fmt.Println("Source type is 'chan'")
	PrintUnderlyingType(make(chan int))
	fmt.Println()

	fmt.Println("Source type is 'float64'")
	PrintUnderlyingType(3.)
	fmt.Println()
}
