package tasks

import "fmt"

// Precondition: slice is already sorted
func BinarySearch[T Ordered](slice []T, value *T) int {
	first := 0
	last := len(slice) - 1

	for last >= first {
		pos := (first + last) / 2
		switch val := slice[pos]; {
		case val == *value:
			return pos
		case val < *value:
			first = pos + 1
		case val > *value:
			last = pos - 1
		}
	}

	return -1
}

func Task17() {
	slice := []int{-7, 0, 1, 1, 2, 2, 2, 4, 4, 5, 6, 9, 11}
	val := 1
	fmt.Println("Actual index: 2, found index:", BinarySearch(slice, &val))

	val = -2
	fmt.Println("No such element, found index:", BinarySearch(slice, &val))

	val = 12
	fmt.Println("No such element, found index:", BinarySearch(slice, &val))
}
