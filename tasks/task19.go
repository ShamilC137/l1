package tasks

import (
	"fmt"
	"strings"
)

func Reverse(value string) string {
	runes := []rune(value)
	last := len(runes) - 1

	builder := strings.Builder{}
	builder.Grow(last + 1)
	for last >= 0 {
		builder.WriteRune(runes[last])
		last--
	}

	return builder.String()
}

func Task19() {
	source := "abcd£efg"
	fmt.Println("Reverse:", Reverse(source))
}
