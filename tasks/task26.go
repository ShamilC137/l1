package tasks

import (
	"fmt"
	"strings"
)

func IsUnique(value string) bool {
	syms := make(map[string]Dummy)

	for _, char := range value {
		symbol := strings.ToLower(string(char))
		if _, found := syms[symbol]; found {
			return false
		} else {
			syms[symbol] = Dummy{}
		}
	}
	return true
}

func Task26() {
	example := "abcd"
	fmt.Println("value:", example)
	fmt.Println("result:", IsUnique(example))
	fmt.Println()

	example = "abCdefAaf"
	fmt.Println("value:", example)
	fmt.Println("result:", IsUnique(example))
	fmt.Println()

	example = "aabcd"
	fmt.Println("value:", example)
	fmt.Println("result:", IsUnique(example))
	fmt.Println()
}
