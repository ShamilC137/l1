package tasks

import "fmt"

func SumCalculateAwaiting(values []int) {
	c := make(chan int, 1)

	go SumCalculate(values, c)

	fmt.Println(<-c)
}

func SumCalculate(values []int, c chan int) {
	sum := 0
	for _, value := range values {
		sum += value * value
	}

	c <- sum
}

func Task3() {
	values := []int{2, 4, 6, 8, 10}
	SumCalculateAwaiting(values)
}
