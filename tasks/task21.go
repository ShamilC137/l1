package tasks

import "fmt"

// Допустим имеется три пакета:
// 1) 1 package содержит некоторый класс Vector, представляющий собой динамический массив (обертка на slice, в которую
// добавлена функция Delete)
// 2) 2 package содержит интерфейс Queue
// 3) 3 package - это пакет, в котором мы на данный момент работаем
// Доступ имеется только к пакету 3 (т.е. остальные read-only). Встала следующая задача: использовать контейнер Vector
// в качестве Queue. Для решения данной задачи был реализован адаптер, который преобразовывает API Vector к Queue.

// 1 package

type Vector[T any] struct {
	data []T
}

func (vector *Vector[T]) Append(value T) {
	vector.data = append(vector.data, value)
}

func (vector *Vector[T]) Delete(pos int) {
	vector.data = Erase(vector.data, pos)
}

func (vector *Vector[T]) Get(pos int) T {
	return vector.data[pos]
}

func (vector *Vector[T]) Len() int {
	return len(vector.data)
}

func NewVector[T any](cap int) *Vector[T] {
	return &Vector[T]{make([]T, 0, cap)}
}

// 2 package

type Queue[T any] interface {
	Pop() T
	Push(val T)
}

// 3 package

type VectorAdapter[T any] struct {
	*Vector[T]
}

func (adapter *VectorAdapter[T]) Pop() T {
	pos := adapter.Len() - 1
	result := adapter.Get(pos)
	adapter.Delete(pos)
	return result
}

func (adapter *VectorAdapter[T]) Push(val T) {
	adapter.Append(val)
}

func Task21() {
	vector := NewVector[int](0)

	adapter := &VectorAdapter[int]{vector}

	var queue Queue[int] = adapter

	queue.Push(1)
	fmt.Println("Pop:", queue.Pop())
}
