package tasks

import "fmt"

type Dummy = struct{}

// O(n)
func intersect[T comparable](lhs, rhs []T) []T {
	m := make(map[T]Dummy)

	// Сливаем значения слайса меньшего размера в таблицу
	dummy := Dummy{}
	for _, value := range lhs {
		m[value] = dummy
	}

	// Проходимся по всем значениям второго слайса. В результирующий слайс кладем только те элементы второго массива,
	// которые содержаться в мапе
	result := make([]T, 0, len(m))
	for _, value := range rhs {
		if _, found := m[value]; found {
			result = append(result, value)
		}
	}

	return result
}

func Intersection(lhs, rhs []int) []int {
	if len(lhs) > len(rhs) {
		return intersect(rhs, lhs)
	} else {
		return intersect(lhs, rhs)
	}
}

func Task11() {
	fmt.Println(Intersection([]int{1, 2, 3, 4, 5, 6},
		[]int{-2, 0, 2, 4, 6, 8, 10}))
}
