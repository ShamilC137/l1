package tasks

import "fmt"

func Delete[T any](slice []T, index int) []T {
	return append(slice[:index], slice[index+1:]...)
}

func Remove[T any](slice []T, index int) []T {
	length := len(slice)
	for index := index; index < length-1; index++ {
		slice[index] = slice[index+1]
	}

	return slice[:length-1]
}

func Erase[T any](slice []T, index int) []T {
	copy(slice[index:], slice[index+1:])
	return slice[:len(slice)-1]
}

// Creates new slice without element at the specified index
func RemoveWithCreate[T any](slice []T, index int) []T {
	result := make([]T, 0, len(slice)-1)
	result = append(result, slice[:index]...)
	result = append(result, slice[index+1:]...)
	return result
}

func Task23() {
	source := []int{1, 2, 3, 4, 5}
	fmt.Println("Source:", source)
	fmt.Println()

	source = Delete(source, 1)
	fmt.Println("After 'Delete'  (index = 1):", source)
	fmt.Println()

	source = Remove(source, 0)
	fmt.Println("After 'Remove' (index = 0):", source)
	fmt.Println()

	source = Erase(source, 2)
	fmt.Println("After 'Erase' (index = 2):", source)
	fmt.Println()

	source = RemoveWithCreate(source, 1)
	fmt.Println("After 'RemoveWithCreate' (index = 1):", source)
	fmt.Println()
}
