package tasks

import "fmt"

// Явно переданный слайс значений можно заменить на канал и средство завершения работы функции (например, Context).
// В данном случае потребуется выбрать размер буфера и заменить два цикла на select и цикл
func Conveyor(sequence []int) {
	// in case of unlimited sequence we can use the appropriate buffer size
	seqLen := len(sequence)
	sourceChan := make(chan int, seqLen)
	calcChan := make(chan int, seqLen)

	go MultiplyCalculator(sourceChan, calcChan)
	for _, val := range sequence {
		sourceChan <- val
	}

	for index := 0; index < seqLen; index++ {
		fmt.Println(<-calcChan)
	}

	close(sourceChan)
}

func MultiplyCalculator(readChan, writeChan chan int) {
	for {
		value, ok := <-readChan
		if !ok {
			return
		}

		writeChan <- value * 2
	}
}

func Task9() {
	Conveyor([]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
}
