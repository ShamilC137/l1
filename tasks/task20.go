package tasks

import (
	"fmt"
	"strings"
)

func ReverseText(text string) string {
	builder := strings.Builder{}
	builder.Grow(len(text))
	elems := strings.Split(text, " ")
	for index := len(elems) - 1; index >= 0; index-- {
		builder.WriteString(elems[index])
		builder.WriteByte(32)
	}

	return builder.String()
}

func Task20() {
	example := "snow dog sun"
	fmt.Println(ReverseText(example))
}
