package tasks

import "fmt"

// package "constraints" is not available up to now
type Signed interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64
}

type Unsigned interface {
	~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr
}

type Integer interface {
	Signed | Unsigned
}

type Float interface {
	~float32 | ~float64
}

type Ordered interface {
	Integer | Float | ~string
}

// суть сортировки: все элементы больше опорного мы переносим в правую часть; все элементы меньше опорного - в левую.
// В худшем случае, при использовании текущего метода выбора опорного элемента, асимптотическая сложность - O(n^2).
// Средняя сложность - O(n*log(n))
func quickSortImpl[T Ordered](slice []T) {
	left := 0
	end := len(slice) - 1
	right := end

	middle := slice[right/2] // опорный элемент

	for left < right {
		// пропускаем элементы, которые стоят на правильных местах
		for ; slice[left] < middle; left++ {
		}
		for ; middle < slice[right]; right-- {
		}

		// элементы, стоящие на неправильных местах, меняем местами
		if left < right {
			slice[left], slice[right] = slice[right], slice[left]
			left++
			right--
		} else if left == right {
			left++
			right--
		}
	}

	// рекурсивно проходимся по оставшимся частям
	if 0 < right {
		quickSortImpl(slice[:right+1])
	}

	if left < end {
		quickSortImpl(slice[left:])
	}
}

func QuickSort[T Ordered](slice []T) {
	if slice == nil {
		return
	}

	length := len(slice)
	if length == 0 {
		return
	}

	quickSortImpl(slice)
}

func Task16() {
	slice := []int{-7, 1, 0, 6, 4, 5, 9, 11, 2, 1, 4, 2, 2}
	fmt.Println("Source vector:", slice)
	QuickSort(slice)
	fmt.Println("Sorted vector:", slice)
}
