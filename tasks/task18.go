package tasks

import (
	"fmt"
	"sync"
	"sync/atomic"
)

type atomicCounter struct {
	counter *int32
}

func (counter atomicCounter) increment() int32 {
	// атомарная операция
	return atomic.AddInt32(counter.counter, 1)
}

func StartAtomicCounter(workerCount uint8) {
	var val int32 = 0
	counter := atomicCounter{&val}

	// Данный объект используется для ожидания завершения работы всех горутин
	wg := sync.WaitGroup{}
	wg.Add(int(workerCount))
	for index := uint8(0); index < workerCount; index++ {
		go func() {
			counter.increment() // незащищенный доступ; безопасно
			wg.Done()
		}()
	}
	wg.Wait()

	// вместо использования WaitGroup объект можно использоваться counter и крутиться в активном ожидании,
	// при помощи Load загружая текущее значения текущее значение и сравнивая его в количеством запущенных горутин

	fmt.Println("Counter value:", *counter.counter) // no need for Load
}

func Task18() {
	count := uint8(8)
	fmt.Println("Worker count:", count)
	StartAtomicCounter(count)
}
